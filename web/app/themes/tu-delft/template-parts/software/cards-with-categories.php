<?php $theme_url = get_template_directory_uri() ?>
<section class="cards-with-categories">
    <div class="cards-with-categories__categories categories" data-scrollbar>
        <div class="categories__wrapper flex">
        <div class="categories__item categories__item--active transition" data-category-target="chapter-0">
            <a href="#">All
            <span class="categories__bg"></span>
            <span class="categories__text"><span>All</span></span>
            </a>
        </div>
        <div class="categories__item" data-category-target="chapter-1">
            <a href="#">A B C
            <span class="categories__bg"></span>
            <span class="categories__text"><span>A B C</span></span>
            </a>
        </div>
        <div class="categories__item" data-category-target="chapter-2">
            <a href="#">D E F
            <span class="categories__bg"></span>
            <span class="categories__text"><span>D E F</span></span></a>
        </div>
        <div class="categories__item" data-category-target="chapter-3">
            <a href="#">G H I
            <span class="categories__bg"></span>
            <span class="categories__text"><span>G H I</span></span>
        </a>
        </div>
        <div class="categories__item" data-category-target="chapter-4">
            <a href="#">J K L
            <span class="categories__bg"></span>
            <span class="categories__text"><span>J K L</span></span>
        </a>
        </div>
        <div class="categories__item" data-category-target="chapter-5">
            <a href="#">M N O
            <span class="categories__bg"></span>
            <span class="categories__text"><span>M N O</span></span>
        </a>
        </div>
        <div class="categories__item" data-category-target="chapter-6">
            <a href="#">P Q R
            <span class="categories__bg"></span>
            <span class="categories__text"><span>P Q R</span></span>
        </a>
        </div>
        <div class="categories__item" data-category-target="chapter-7">
            <a href="#">S T U
            <span class="categories__bg"></span>
            <span class="categories__text"><span>S T U</span></span>
        </a>
        </div>
        <div class="categories__item" data-category-target="chapter-8">
            <a href="#">V W X
            <span class="categories__bg"></span>
            <span class="categories__text"><span>V W X</span></span>
        </a>
        </div>
        <div class="categories__item" data-category-target="chapter-9">
            <a href="#">Y Z
            <span class="categories__bg"></span>
            <span class="categories__text"><span>Y Z</span></span>
        </a>
        </div>

        </div>
    </div>
    <div class="cards-with-categories__wrapper">
        <div class="cards-with-categories__content active" data-category-content="chapter-0" data-pages>
            <div class="grid-links grid-links--three-columns grid lg:grid-cols-2">
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>

            </div>
            <div class="cards-with-categories__pagination pagination flex items-center justify-center">
                <a href="#" class="pagination__button pagination__button--prev flex items-center justify-center">
                    <svg width="20" height="20">
                        <use href="<?= get_template_directory_uri() ?>/src/sprite.svg#arrow-right"></use>
                    </svg>

                </a>
                <div class="pagination__list">
                    <div class="pagination__bg"></div>
                    <ul class="flex items-center justify-center">

                    </ul>
                </div>
                <a href="#" class="pagination__button pagination__button--next flex items-center justify-center">
                    <svg width="20" height="20">
                        <use href="<?= get_template_directory_uri() ?>/src/sprite.svg#arrow-right"></use>
                    </svg>
                    
                </a>
            </div>
        </div>
        <div class="cards-with-categories__content" data-category-content="chapter-1" data-pages>
            <div class="grid-links grid-links--three-columns grid lg:grid-cols-2">
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>

            </div>
            <div class="cards-with-categories__pagination pagination flex items-center justify-center">
                <a href="#" class="pagination__button pagination__button--prev flex items-center justify-center">
                    <svg width="20" height="20">
                        <use href="<?= get_template_directory_uri() ?>/src/sprite.svg#arrow-right"></use>
                    </svg>

                </a>
                <div class="pagination__list">
                    <div class="pagination__bg"></div>
                    <ul class="flex items-center justify-center">

                    </ul>
                </div>
                <a href="#" class="pagination__button pagination__button--next flex items-center justify-center">
                    <svg width="20" height="20">
                        <use href="<?= get_template_directory_uri() ?>/src/sprite.svg#arrow-right"></use>
                    </svg>
                    
                </a>
            </div>
        </div>
        <div class="cards-with-categories__content" data-category-content="chapter-2" data-pages>
            <div class="grid-links grid-links--three-columns grid lg:grid-cols-2">
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>

            </div>
            <div class="cards-with-categories__pagination pagination flex items-center justify-center">
                <a href="#" class="pagination__button pagination__button--prev flex items-center justify-center">
                    <svg width="20" height="20">
                        <use href="<?= get_template_directory_uri() ?>/src/sprite.svg#arrow-right"></use>
                    </svg>

                </a>
                <div class="pagination__list">
                    <div class="pagination__bg"></div>
                    <ul class="flex items-center justify-center">

                    </ul>
                </div>
                <a href="#" class="pagination__button pagination__button--next flex items-center justify-center">
                    <svg width="20" height="20">
                        <use href="<?= get_template_directory_uri() ?>/src/sprite.svg#arrow-right"></use>
                    </svg>
                    
                </a>
            </div>
        </div>
        <div class="cards-with-categories__content" data-category-content="chapter-3" data-pages>
            <div class="grid-links grid-links--three-columns grid lg:grid-cols-2">
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>

            </div>
            <div class="cards-with-categories__pagination pagination flex items-center justify-center">
                <a href="#" class="pagination__button pagination__button--prev flex items-center justify-center">
                    <svg width="20" height="20">
                        <use href="<?= get_template_directory_uri() ?>/src/sprite.svg#arrow-right"></use>
                    </svg>

                </a>
                <div class="pagination__list">
                    <div class="pagination__bg"></div>
                    <ul class="flex items-center justify-center">

                    </ul>
                </div>
                <a href="#" class="pagination__button pagination__button--next flex items-center justify-center">
                    <svg width="20" height="20">
                        <use href="<?= get_template_directory_uri() ?>/src/sprite.svg#arrow-right"></use>
                    </svg>
                    
                </a>
            </div>
        </div>
        <div class="cards-with-categories__content" data-category-content="chapter-4" data-pages>
            <div class="grid-links grid-links--three-columns grid lg:grid-cols-2">
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>

            </div>
            <div class="cards-with-categories__pagination pagination flex items-center justify-center">
                <a href="#" class="pagination__button pagination__button--prev flex items-center justify-center">
                    <svg width="20" height="20">
                        <use href="<?= get_template_directory_uri() ?>/src/sprite.svg#arrow-right"></use>
                    </svg>

                </a>
                <div class="pagination__list">
                    <div class="pagination__bg"></div>
                    <ul class="flex items-center justify-center">

                    </ul>
                </div>
                <a href="#" class="pagination__button pagination__button--next flex items-center justify-center">
                    <svg width="20" height="20">
                        <use href="<?= get_template_directory_uri() ?>/src/sprite.svg#arrow-right"></use>
                    </svg>
                    
                </a>
            </div>
        </div>
        <div class="cards-with-categories__content" data-category-content="chapter-5" data-pages>
            <div class="grid-links grid-links--three-columns grid lg:grid-cols-2">
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>

            </div>
            <div class="cards-with-categories__pagination pagination flex items-center justify-center">
                <a href="#" class="pagination__button pagination__button--prev flex items-center justify-center">
                    <svg width="20" height="20">
                        <use href="<?= get_template_directory_uri() ?>/src/sprite.svg#arrow-right"></use>
                    </svg>

                </a>
                <div class="pagination__list">
                    <div class="pagination__bg"></div>
                    <ul class="flex items-center justify-center">

                    </ul>
                </div>
                <a href="#" class="pagination__button pagination__button--next flex items-center justify-center">
                    <svg width="20" height="20">
                        <use href="<?= get_template_directory_uri() ?>/src/sprite.svg#arrow-right"></use>
                    </svg>
                    
                </a>
            </div>
        </div>
        <div class="cards-with-categories__content" data-category-content="chapter-6" data-pages>
            <div class="grid-links grid-links--three-columns grid lg:grid-cols-2">
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>

            </div>
            <div class="cards-with-categories__pagination pagination flex items-center justify-center">
                <a href="#" class="pagination__button pagination__button--prev flex items-center justify-center">
                    <svg width="20" height="20">
                        <use href="<?= get_template_directory_uri() ?>/src/sprite.svg#arrow-right"></use>
                    </svg>

                </a>
                <div class="pagination__list">
                    <div class="pagination__bg"></div>
                    <ul class="flex items-center justify-center">

                    </ul>
                </div>
                <a href="#" class="pagination__button pagination__button--next flex items-center justify-center">
                    <svg width="20" height="20">
                        <use href="<?= get_template_directory_uri() ?>/src/sprite.svg#arrow-right"></use>
                    </svg>
                    
                </a>
            </div>
        </div>
        <div class="cards-with-categories__content" data-category-content="chapter-7" data-pages>
            <div class="grid-links grid-links--three-columns grid lg:grid-cols-2">
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>

            </div>
            <div class="cards-with-categories__pagination pagination flex items-center justify-center">
                <a href="#" class="pagination__button pagination__button--prev flex items-center justify-center">
                    <svg width="20" height="20">
                        <use href="<?= get_template_directory_uri() ?>/src/sprite.svg#arrow-right"></use>
                    </svg>

                </a>
                <div class="pagination__list">
                    <div class="pagination__bg"></div>
                    <ul class="flex items-center justify-center">

                    </ul>
                </div>
                <a href="#" class="pagination__button pagination__button--next flex items-center justify-center">
                    <svg width="20" height="20">
                        <use href="<?= get_template_directory_uri() ?>/src/sprite.svg#arrow-right"></use>
                    </svg>
                    
                </a>
            </div>
        </div>
        <div class="cards-with-categories__content" data-category-content="chapter-8" data-pages>
            <div class="grid-links grid-links--three-columns grid lg:grid-cols-2">
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>

            </div>
            <div class="cards-with-categories__pagination pagination flex items-center justify-center">
                <a href="#" class="pagination__button pagination__button--prev flex items-center justify-center">
                    <svg width="20" height="20">
                        <use href="<?= get_template_directory_uri() ?>/src/sprite.svg#arrow-right"></use>
                    </svg>

                </a>
                <div class="pagination__list">
                    <div class="pagination__bg"></div>
                    <ul class="flex items-center justify-center">

                    </ul>
                </div>
                <a href="#" class="pagination__button pagination__button--next flex items-center justify-center">
                    <svg width="20" height="20">
                        <use href="<?= get_template_directory_uri() ?>/src/sprite.svg#arrow-right"></use>
                    </svg>
                    
                </a>
            </div>
        </div>
        <div class="cards-with-categories__content" data-category-content="chapter-9" data-pages>
            <div class="grid-links grid-links--three-columns grid lg:grid-cols-2">
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>
                <a href="#" class="software-link" data-card>
                    <div class="software-link__wrapper">
                        <div class="software-link__row  flex items-start">
                            <h6>
                            AutoCAD
                            </h6>
                            <figure class="software-link__icon">
                                <img width="44" height="40" src="<?= $theme_url ?>/src/img/icons/btn-a.svg" alt="image">
                            </figure>
                        </div>
                        <div class="arrow">
                            <svg width="14" height="22">
                                <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                            </svg>
                            <svg width="38" height="3">
                                <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                            </svg>
                        </div>
                    </div>
                </a>

            </div>
            <div class="cards-with-categories__pagination pagination flex items-center justify-center">
                <a href="#" class="pagination__button pagination__button--prev flex items-center justify-center">
                    <svg width="20" height="20">
                        <use href="<?= get_template_directory_uri() ?>/src/sprite.svg#arrow-right"></use>
                    </svg>

                </a>
                <div class="pagination__list">
                    <div class="pagination__bg"></div>
                    <ul class="flex items-center justify-center">

                    </ul>
                </div>
                <a href="#" class="pagination__button pagination__button--next flex items-center justify-center">
                    <svg width="20" height="20">
                        <use href="<?= get_template_directory_uri() ?>/src/sprite.svg#arrow-right"></use>
                    </svg>
                    
                </a>
            </div>
        </div>
    </div>
</section>