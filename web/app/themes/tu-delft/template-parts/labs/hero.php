<?php $theme_url = get_template_directory_uri() ?>
<section class="hero" >
    <div class="hero__container container md:flex items-end justify-between">
        <div class="hero__content">
            <h1>Labs</h1>
            <p>Find hands-on manuals and important information related to the use of the different labs at TU Delft.</p>
        </div>
        <figure class="hero__image">
            <img width="288" height="224" src="<?= $theme_url ?>/src/img/hero-labs.svg" alt="image">
        </figure>
    </div>
</section>