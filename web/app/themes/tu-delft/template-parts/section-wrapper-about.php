<?php $theme_url = get_template_directory_uri() ?>
<section class="section-wrapper">
    <div class="section-wrapper__text">
        <div class="text">
            <h2>DIGIPEDIA</h2>
            <h3>About us</h3>
            <p>
            Lorem ipsum dolor sit amet consectetur. Sed dui risus et habitasse consectetur leo. Ac placerat aliquam nulla suspendisse semper. Auctor egestas aliquet adipiscing eget. At purus ornare volutpat sed in habitasse ligula. Diam ipsum facilisi morbi dapibus nullam tortor vestibulum interdum. Ac est gravida id sit aliquam diam vulputate scelerisque massa.
                    </p>
        </div>
        <div class="image">
            <figure>
                <img data-image-src="<?= $theme_url ?>/src/img/tutorial/img-1.jpg" width="808" height="454" src="<?= $theme_url ?>/src/img/tutorial/img-1.jpg" alt="image">
                <figcaption>
                Image From: Murphy, K. P. (2021). Figure 14.4. In Machine Learning: A Probabilistic Perspective. textbook, MIT Press.
                </figcaption>
            </figure>
        </div>
        <div class="text">
            <h4>Title Text</h4>
            <p>
                <small>
                Lorem ipsum dolor sit amet consectetur. Sed dui risus et habitasse consectetur leo. Ac placerat aliquam nulla suspendisse semper. Auctor egestas aliquet adipiscing eget. At purus ornare volutpat sed in habitasse ligula. Diam ipsum facilisi morbi dapibus nullam tortor vestibulum interdum. Ac est gravida id sit aliquam diam vulputate scelerisque massa.
                </small>
            </p>
        </div>
        <div class="text">
            <h4>Give Feedback</h4>
            <p>
                <small>
                Lorem ipsum dolor sit amet consectetur. Sed dui risus et habitasse consectetur leo. Ac placerat aliquam nulla suspendisse semper. Auctor egestas aliquet adipiscing eget. At purus ornare volutpat sed in habitasse ligula. Diam ipsum facilisi morbi dapibus nullam tortor vestibulum interdum. Ac est gravida id sit aliquam diam vulputate scelerisque massa.
                </small>
            </p>
            <a href="#" class="btn btn--full-width-onmobile" data-prev>
                <span>Give feedback</span>
                <span>Give feedback</span>
            </a>
        </div>
    </div>
</section>