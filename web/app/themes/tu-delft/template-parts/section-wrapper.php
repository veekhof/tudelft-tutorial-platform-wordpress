<?php $theme_url = get_template_directory_uri() ?>
<section class="section-wrapper">
    <div class="section-wrapper__title">
        <h2>Navigate our tutorials</h2>
    </div>
        <div class="grid-links grid lg:grid-cols-2">
                    <a href="#" class="card-with-image card-with-image--reverse card-with-image--not-full-image">
                        <div class="card-with-image__wrapper sm:flex">
                            <figure class="card-with-image__image">
                                <img  width="204" height="160" src="<?= $theme_url ?>/src/img/card-with-image/courses.svg" alt="image">
                            </figure>
                            <div class="card-with-image__content">
                                <h4>Courses</h4>
                                <p>
                                Explore customised tutorials designed to meet the distinctive needs of TU Delft's educational courses.
                                </p>
                                <div class="arrow">
                                    <svg width="14" height="22">
                                        <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                                    </svg>
                                    <svg width="38" height="3">
                                        <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                                    </svg>
                                </div>
                            </div>
                        </div>
                    </a>
                    <a href="#" class="card-with-image card-with-image--reverse card-with-image--not-full-image">
                        <div class="card-with-image__wrapper sm:flex">
                            <figure class="card-with-image__image">
                                <img  width="204" height="160" src="<?= $theme_url ?>/src/img/card-with-image/subjects.svg" alt="image">
                            </figure>
                            <div class="card-with-image__content">
                                <h4>Software</h4>
                                <p>
                                Explore software-specific tutorials that align perfectly with the technological landscape of TU Delft's courses.
                                </p>
                                <div class="arrow">
                                    <svg width="14" height="22">
                                        <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                                    </svg>
                                    <svg width="38" height="3">
                                        <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                                    </svg>
                                </div>
                            </div>
                        </div>
                    </a>
                    <a href="#" class="card-with-image card-with-image--reverse card-with-image--not-full-image">
                        <div class="card-with-image__wrapper sm:flex">
                            <figure class="card-with-image__image">
                                <img  width="204" height="160" src="<?= $theme_url ?>/src/img/card-with-image/software.svg" alt="image">
                            </figure>
                            <div class="card-with-image__content">
                                <h4>Software</h4>
                                <p>
                                Explore software-specific tutorials that align perfectly with the technological landscape of TU Delft's courses.
                                </p>
                                <div class="arrow">
                                    <svg width="14" height="22">
                                        <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                                    </svg>
                                    <svg width="38" height="3">
                                        <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                                    </svg>
                                </div>
                            </div>
                        </div>
                    </a>
                    <a href="#" class="card-with-image card-with-image--reverse card-with-image--not-full-image">
                        <div class="card-with-image__wrapper sm:flex">
                            <figure class="card-with-image__image">
                                <img  width="204" height="160" src="<?= $theme_url ?>/src/img/card-with-image/labs.svg" alt="image">
                            </figure>
                            <div class="card-with-image__content">
                                <h4>Labs</h4>
                                <p>
                                Find hands-on manuals and important information related to the use of the different labs at TU Delft.
                                </p>
                                <div class="arrow">
                                    <svg width="14" height="22">
                                        <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                                    </svg>
                                    <svg width="38" height="3">
                                        <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                                    </svg>
                                </div>
                            </div>
                        </div>
                    </a>

        </div>
</section>