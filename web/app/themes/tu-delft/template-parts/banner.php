<?php $theme_url = get_template_directory_uri() ?>
<section class="banner">
    <div class="banner__wrapper md:flex items-start justify-between">
        <figure class="banner__bg">
            <img  width="1224" height="272" src="<?= $theme_url ?>/src/img/banner.jpg" alt="image">
        </figure>
        <div class="banner__title">
            <h1>
                <small>Welcome to DigiPedia</small>
                The new Tutorials Platform for you
            </h1>
        </div>
        <div class="banner__content">
            <p>
            Your gateway to mastering cutting-edge tools and technologies at your own pace, propelling your skills to new heights
            </p>
            <div class="banner__link">
                <a href="#" class="link link--white" >About DigiPedia</a>
            </div>
        </div>
    </div>
</section>