<section class="hero-text">
    <div class="hero-text__content">
        <h2>Skills that drive you forward</h2>
        <p>
        Your gateway to mastering cutting-edge tools and technologies at your own pace, propelling your skills to new heights
        </p>
        <div class="hero-text__link">
            <a href="#" class="link">About DigiPedia</a>
        </div>
    </div>
</section>