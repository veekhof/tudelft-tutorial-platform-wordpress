<?php $theme_url = get_template_directory_uri() ?>
<section class="hero" >
    <div class="hero__container container md:flex items-end justify-between">
        <div class="hero__content">
            <h1>Software</h1>
            <p>Dive into tailored tutorials designed to meet the unique requirements of subjects at TU Delft.</p>
        </div>
        <figure class="hero__image">
            <img width="288" height="224" src="<?= $theme_url ?>/src/img/hero-software.svg" alt="image">
        </figure>
    </div>
</section>