<?php $theme_url = get_template_directory_uri() ?>
<section class="section-wrapper">
    <div class="section-wrapper__title">
        <h2>Recently added tutorials</h2>
    </div>
        <div class="grid-links grid lg:grid-cols-2">
                    <a href="#" class="card-with-image">
                        <div class="card-with-image__wrapper sm:flex">
                            <figure class="card-with-image__image">
                                <img  width="208" height="280" src="<?= $theme_url ?>/src/img/card-with-image/2d.jpg" alt="image">
                            </figure>
                            <div class="card-with-image__content">
                                <h4>2D</h4>
                                <p>
                                Lorem ipsum dolor sit amet consectetur. Condimentum adipiscing nisl amet egestas vulputate consequat. In in at turpis facilisi eget in.
                                </p>
                                <div class="arrow">
                                    <svg width="14" height="22">
                                        <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                                    </svg>
                                    <svg width="38" height="3">
                                        <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                                    </svg>
                                </div>
                            </div>
                        </div>
                    </a>
                    <a href="#" class="card-with-image">
                        <div class="card-with-image__wrapper sm:flex">
                            <figure class="card-with-image__image">
                                <img  width="208" height="280" src="<?= $theme_url ?>/src/img/card-with-image/2d.jpg" alt="image">
                            </figure>
                            <div class="card-with-image__content">
                                <h4>2D</h4>
                                <p>
                                Lorem ipsum dolor sit amet consectetur. Condimentum adipiscing nisl amet egestas vulputate consequat. In in at turpis facilisi eget in.
                                </p>
                                <div class="arrow">
                                    <svg width="14" height="22">
                                        <use href="<?= $theme_url ?>/src/sprite.svg#arrow-large"></use>
                                    </svg>
                                    <svg width="38" height="3">
                                        <use href="<?= $theme_url ?>/src/sprite.svg#line"></use>
                                    </svg>
                                </div>
                            </div>
                        </div>
                    </a>

        </div>
</section>