<?php $theme_url = get_template_directory_uri() ?>
<section class="hero hero--center-image-onmobile" >
    <div class="hero__container container md:flex items-end justify-between">
        <div class="hero__content">
            <h1>Courses</h1>
            <p>Explore customised tutorials designed to meet the distinctive needs of TU Delft's educational courses.</p>
        </div>
        <figure class="hero__image">
            <img width="288" height="224" src="<?= $theme_url ?>/src/img/hero.svg" alt="image">
        </figure>
    </div>
</section>